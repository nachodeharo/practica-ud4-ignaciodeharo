package com.ignacio;

import com.ignacio.gui.Controlador;
import com.ignacio.gui.Modelo;
import com.ignacio.gui.Vista;

/**
 *
 * Trabajo de bases de datos sobre mi tematica Clientes/Instructores, añadiendo el sistema MVC.
 * En principio tiene todos los puntos extra requerridos
 * @author Nacho De Haro / SrWolfMoon
 * @see Controlador
 * @see Modelo
 * @see com.ignacio.clases.Socio
 * @see Vista
 * @see com.ignacio.clases.Instructor
 * @see com.ignacio.clases.Actividad
 * @see com.ignacio.clases.Enums.LugarAct
 * @see com.ignacio.utils.Util
 * @version 26/02/2021   1.0
 * @since 1.8
 *
 */
public class main {
    public static void main(String[] args) {
        new Controlador(new Modelo(), new Vista());
    }
}
