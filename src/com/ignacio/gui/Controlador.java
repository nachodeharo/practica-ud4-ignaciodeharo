package com.ignacio.gui;

import com.ignacio.clases.Actividad;
import com.ignacio.clases.Instructor;
import com.ignacio.clases.Socio;
import com.ignacio.utils.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Clase Controlador que nos permitirá conectar con el modelo y la vista, aqui se operará todo en esencia
 */
public class Controlador implements ActionListener, ListSelectionListener {
    public Vista vista;
    public Modelo modelo;

    /**
     * Constructor por el que se instancia la case Controlador
     * @param modelo objeto de la clase Modelo
     * @param vista objeto de la clase Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("GYM - <CONECTADO>");
            listarTodo();
            //setBotonesActivados(true);
            //listarProductos();
            //listarEmpleados();
            //listarDepartamentos();
        } catch (Exception ex) {
            Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
        }
    }

    /**
     * Permite a los botones ser escuchados y usados
     * @param listener el ActionListner para ser utilizado (en mi caso la clase)
     */
    private void addActionListeners(ActionListener listener){
        vista.clienteAltaBtn.addActionListener(listener);
        vista.clienteModBtn.addActionListener(listener);
        vista.clienteElimBtn.addActionListener(listener);
        vista.instructorAltaBtn.addActionListener(listener);
        vista.instructorModBtn.addActionListener(listener);
        vista.instructorElimBtn.addActionListener(listener);
        vista.actividadAltaBtn.addActionListener(listener);
        vista.actividadModBtn.addActionListener(listener);
        vista.actividadElimBtn.addActionListener(listener);
        vista.InstverSociosButton.addActionListener(listener);
        vista.InstverActButton.addActionListener(listener);
        vista.clienteBuscarBtn.addActionListener(listener);
        vista.listarSocios.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    /**
     * Permite a las listas ser escuchadas y usadas
     * @param listener el ActionListner para ser utilizado (en mi caso la clase)
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.clienteLista.addListSelectionListener(listener);
        vista.instList.addListSelectionListener(listener);
        vista.actList.addListSelectionListener(listener);
    }


    /**
     * Nos permitirá reaccionar a los distintos botones de Alta, Modificación, Eliminación, acceso a menús...
     * Y posteriormente conectará al modelo que a su vez conectará con la base de datos (Todo automatizado)
     * @param e el evento seleccionado (Un botón por ejemplo)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.frame.setTitle("GYM- <CONECTADO>");
                        //setBotonesActivados(true);
                        listarTodo();
                    } else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.frame.setTitle("GYM- <SIN CONEXION>");
                        //setBotonesActivados(false);
                        vista.dtmCliente.clear();
                        vista.dtmInstructor.clear();
                        vista.dtmActividad.clear();
                        vista.dtmInstVerActividades.clear();
                        vista.dtmInstVerSocios.clear();
                        listarTodo();
                    }
                } catch (Exception ex) {

                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;
                //**************
            case "clienteAltaBtn":
                if (comprobarCamposSocios()) {
                    Socio socio = new Socio();
                    socio.setNombre(vista.txtClienteNombre.getText());
                    socio.setApellidos(vista.txtClienteApellidos.getText());
                    socio.setDni(vista.txtClienteDni.getText());
                    socio.setFechaAlta(vista.clienteDatePicker.getDate());
                    Instructor ins = (Instructor) vista.clienteCombo.getSelectedItem();
                    socio.setIdInstructor(ins.getId()+"");

                    modelo.guardarObjeto(socio);
                    listarTodo();
                    limpiarCamposCliente();
                }else{
                    Util.mostrarMensajeError("RELLENA TODO");
                }
                break;
            case "clienteModBtn":
                if (vista.clienteLista.getSelectedValue() != null) {
                    if (comprobarCamposSocios()) {
                        Socio socioMod = (Socio) vista.clienteLista.getSelectedValue();
                        socioMod.setNombre(vista.txtClienteNombre.getText());
                        socioMod.setApellidos(vista.txtClienteApellidos.getText());
                        socioMod.setDni(vista.txtClienteDni.getText());
                        socioMod.setFechaAlta(vista.clienteDatePicker.getDate());
                        Instructor insMod = (Instructor)vista.clienteCombo.getSelectedItem();
                        socioMod.setIdInstructor(insMod.getId()+"");
                        modelo.modificarObjeto(socioMod);
                        limpiarCamposCliente();
                        listarTodo();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }

                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "clienteElimBtn":
                if (vista.clienteLista.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.clienteLista.getSelectedValue());
                    listarTodo();
                    limpiarCamposCliente();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "clienteBuscarBtn":
                String respuesta = Util.preguntarMensaje("Introduce el DNI para el filtrado");

                if (respuesta != null){
                    listarFiltradoSocios(respuesta);
                }
                break;
            case "listarSocios":
                listarSocios();
                limpiarCamposCliente();
                break;
                //**************
            case "instructorAltaBtn":
                if (comprobarCamposInstructor()) {
                    Instructor instructor = new Instructor();
                    instructor.setNombre(vista.txtInstNombre.getText());
                    instructor.setApellidos(vista.txtInstApellidos.getText());
                    instructor.setCodIns(vista.txtInstcodInst.getText());

                    modelo.guardarObjeto(instructor);
                    limpiarCamposInstructor();
                    listarTodo();
                }
                break;
            case "instructorModBtn":
                if (vista.instList.getSelectedValue() != null) {
                    if (comprobarCamposInstructor()) {
                        Instructor instMod = (Instructor) vista.instList.getSelectedValue();
                        instMod.setNombre(vista.txtInstNombre.getText());
                        instMod.setApellidos(vista.txtInstApellidos.getText());
                        instMod.setCodIns(vista.txtInstcodInst.getText());

                        modelo.modificarObjeto(instMod);
                        limpiarCamposInstructor();
                        listarTodo();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }

                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "instructorElimBtn":
                if (vista.instList.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.instList.getSelectedValue());
                    listarTodo();
                    limpiarCamposInstructor();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "InstverSociosButton":
                if (vista.instList.getSelectedValue() != null) {
                    Instructor instFiltro = (Instructor) vista.instList.getSelectedValue();
                    listarSociosBusqueda(modelo.getSocioInstructor(instFiltro.getId()+""));
                    vista.actLblSocios.setText("Socios de " + instFiltro.getNombre() + " " + instFiltro.getApellidos());
                }
                break;
            case "InstverActButton":
                if (vista.instList.getSelectedValue() != null) {
                    Instructor instFiltro = (Instructor) vista.instList.getSelectedValue();
                    listarActividadesBusqueda(modelo.getActividadInstructor(instFiltro.getId()+""));
                    vista.actLblAct.setText("Actividades de " + instFiltro.getNombre() + " " + instFiltro.getApellidos());
                }
                break;
                //******************
            case "actividadAltaBtn":
                if (comprobarCamposActividad()) {
                    Actividad actividad = new Actividad();
                    actividad.setTitulo(vista.txtActTitulo.getText());
                    actividad.setPrecio(Double.parseDouble(vista.txtActPrecio.getText()));
                    actividad.setLugar(String.valueOf(vista.actComboLugar.getSelectedItem()));
                    Instructor insMod = (Instructor)vista.actComboInstructor.getSelectedItem();
                    actividad.setIdInstructor(insMod.getId()+"");

                    modelo.guardarObjeto(actividad);
                    limpiarCamposActividad();
                    listarTodo();
                }
                break;
            case "actividadModBtn":
                if (vista.actList.getSelectedValue() != null) {
                    if (comprobarCamposActividad()) {
                        Actividad actividadMod = (Actividad) vista.actList.getSelectedValue();
                        actividadMod.setTitulo(vista.txtActTitulo.getText());
                        actividadMod.setPrecio(Double.parseDouble(vista.txtActPrecio.getText()));
                        actividadMod.setLugar(String.valueOf(vista.actComboLugar.getSelectedItem()));
                        Instructor insMod = (Instructor)vista.actComboInstructor.getSelectedItem();
                        actividadMod.setIdInstructor(insMod.getId()+"");

                        modelo.modificarObjeto(actividadMod);
                        limpiarCamposActividad();
                        listarTodo();
                    } else {
                        Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "actividadElimBtn":
                if (vista.actList.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.actList.getSelectedValue());
                    listarTodo();
                    limpiarCamposActividad();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
        }
    }

    /**
     * Limpia los campos de la ventana Actividad
     */
    private void limpiarCamposActividad() {
        vista.txtActTitulo.setText("");
        vista.txtActPrecio.setText("");
        vista.actComboLugar.setSelectedIndex(-1);
        vista.actComboInstructor.setSelectedIndex(-1);
    }

    /**
     * Comprueba campos vacíos de Actividad
     * @return true si están todos rellenos
     */
    private boolean comprobarCamposActividad() {

        if (vista.txtActPrecio.getText().isEmpty() || vista.txtActTitulo.getText().isEmpty() ||
            vista.actComboLugar.getSelectedIndex() == -1 || vista.actComboInstructor.getSelectedIndex() == -1){
            return false;
        }
        return true;
    }

    /**
     * Permite limpiar los campos de instructor
     */
    private void limpiarCamposInstructor() {
        vista.txtInstNombre.setText(null);
        vista.txtInstApellidos.setText(null);
        vista.txtInstcodInst.setText(null);
    }

    /**
     * Comprueba si hay algun campo vacío en la clase instructor
     * @return true si están todos rellenos
     */
    private boolean comprobarCamposInstructor() {

        if(vista.txtInstNombre.getText().isEmpty() || vista.txtInstApellidos.getText().isEmpty() ||
            vista.txtInstcodInst.getText().isEmpty()){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Limpia los campos de socios/clientes
     */
    private void limpiarCamposCliente() {
        vista.txtClienteNombre.setText("");
        vista.txtClienteApellidos.setText("");
        vista.txtClienteDni.setText("");
        vista.clienteCombo.setSelectedIndex(-1);
        vista.clienteDatePicker.setText("");
    }

    /**
     * Comprueba si hay campos vacíos en socios
     * @return true si están todos rellenos
     */
    private boolean comprobarCamposSocios() {
        if (vista.txtClienteNombre.getText().isEmpty() || vista.txtClienteApellidos.getText().isEmpty() ||
            vista.txtClienteDni.getText().isEmpty() || vista.clienteCombo.getSelectedIndex() == -1 ||
            vista.clienteDatePicker.getText().isEmpty()){
            System.out.println("entro");
            return false;

        }

        return true;
    }

    /**
     * Lista todas las tablas
     */
    private void listarTodo() {
        listarSocios();
        listarInstructores();
        listarActividades();
    }

    /**
     * Lista todos los filtrados de los socios según un DNI
     * @param dni dni del socio como parametro
     */
    private void listarFiltradoSocios(String dni){
        vista.dtmCliente.clear();
        for (Socio socio : modelo.getSocioDni(dni)) {
            vista.dtmCliente.addElement(socio);
        }
    }

    /**
     * Permite listar instructores en el JList y sus combos
     */
    private void listarInstructores() {
        vista.dtmInstructor.clear();
        vista.clienteCombo.removeAllItems();
        vista.actComboInstructor.removeAllItems();
        for (Instructor instructor : modelo.getInstructor()) {
            vista.dtmInstructor.addElement(instructor);
            vista.clienteCombo.addItem(instructor);
            vista.actComboInstructor.addItem(instructor);
        }
        vista.clienteCombo.setSelectedIndex(-1);
        vista.actComboInstructor.setSelectedIndex(-1);
    }

    /**
     * Permite listar las actividades en el jlist
     */
    private void listarActividades() {
        vista.dtmActividad.clear();
        for (Actividad actividad : modelo.getActividad()) {
            vista.dtmActividad.addElement(actividad);
        }
    }

    /**
     * Permite listar los socios en el jlist
     */
    private void listarSocios() {
        vista.dtmCliente.clear();
        for (Socio socio : modelo.getSocio()) {
            vista.dtmCliente.addElement(socio);
        }
    }

    /**
     * Permite listar los socios en el jlist filtrados por X instructor
     */
    public void listarSociosBusqueda (ArrayList<Socio> lista){
        vista.dtmInstVerSocios.clear();
        for (Socio socio: lista){
            vista.dtmInstVerSocios.addElement(socio);
        }
    }

    /**
     * Permite listar las actividades en el jlist filtrados por un instructor con X codigo
     */
    public void listarActividadesBusqueda (ArrayList<Actividad> lista){
        vista.dtmInstVerActividades.clear();
        for (Actividad act: lista){
            vista.dtmInstVerActividades.addElement(act);
        }
    }

    /**
     * Método sobreescrito que nos permite detectar si hacemos click en una de las listas para auto-rellenar los campos
     * y así tenerlo en los TextFields para sobreescribirlos/modificarlos/borrarlos
     * @param e el evento de lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.clienteLista) {
                if (vista.clienteLista.getSelectedValue() != null) {
                    Socio socio = (Socio) vista.clienteLista.getSelectedValue();
                    vista.txtClienteNombre.setText(socio.getNombre());
                    vista.txtClienteApellidos.setText(socio.getApellidos());
                    vista.txtClienteDni.setText(socio.getDni());
                    vista.clienteDatePicker.setDate(socio.getFechaAlta());
                    //vista.clienteCombo.setSelectedItem(modelo.getInstructorSocio(socio.getIdInstructor()));
                    for (int i = 0 ; i < vista.clienteCombo.getItemCount() ; i++){
                        Instructor ins = (Instructor) vista.clienteCombo.getItemAt(i);
                        String cod = ins.getId() + "";
                        if (cod.equals(socio.getIdInstructor())){
                            vista.clienteCombo.setSelectedIndex(i);
                        }
                    }

                }
            } else if (e.getSource() == vista.instList){
                if (vista.instList.getSelectedValue() != null) {
                    Instructor instructor = (Instructor) vista.instList.getSelectedValue();
                    vista.txtInstNombre.setText(instructor.getNombre());
                    vista.txtInstApellidos.setText(instructor.getApellidos());
                    vista.txtInstcodInst.setText(instructor.getCodIns());
                }
            } else if (e.getSource() == vista.actList){
                if (vista.actList.getSelectedValue() != null) {
                    Actividad act = (Actividad) vista.actList.getSelectedValue();
                    vista.txtActTitulo.setText(act.getTitulo());
                    vista.txtActPrecio.setText(act.getPrecio()+"");
                    vista.actComboLugar.setSelectedItem(act.getLugar());
                    for (int i = 0 ; i < vista.actComboInstructor.getItemCount() ; i++){
                        Instructor ins = (Instructor) vista.actComboInstructor.getItemAt(i);
                        String cod = ins.getId() + "";
                        if (cod.equals(act.getIdInstructor())){
                            vista.actComboInstructor.setSelectedIndex(i);
                        }
                    }
                }
            }
        }
    }
}
