package com.ignacio.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.ignacio.clases.Enums.LugarAct;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista que hereda de JFrame que permite la utilización de la interfaz
 */
public class Vista extends JFrame{
    protected JFrame frame;
    protected JTabbedPane tabbedPane1;
    protected JPanel panel1;
    protected JComboBox clienteCombo;
    protected JButton clienteAltaBtn;
    protected JList clienteLista;
    protected JButton instructorAltaBtn;
    protected JTextField txtActTitulo;
    protected JComboBox actComboLugar;
    protected JButton actividadAltaBtn;
    protected JList actList;
    protected JButton clienteModBtn;
    protected JButton clienteElimBtn;
    protected JButton instructorElimBtn;
    protected JButton actividadModBtn;
    protected JButton actividadElimBtn;
    protected DatePicker clienteDatePicker;
    protected JTextField txtActPrecio;
    protected JComboBox actComboInstructor;
    protected JButton instructorModBtn;
    protected JTextField txtClienteNombre;
    protected JList instList;
    protected JButton InstverSociosButton;
    protected JList instListVerSocios;
    protected JButton InstverActButton;
    protected JList instListVerAct;
    protected JButton clienteBuscarBtn;
    protected JTextField txtInstNombre;
    protected JTextField txtInstApellidos;
    protected JTextField txtInstcodInst;
    protected JTextField txtClienteApellidos;
    protected JTextField txtClienteDni;
    protected JLabel actLblSocios;
    protected JLabel actLblAct;
    protected JButton listarSocios;


    //POR MI ***********************************************************************

    //DTM
    protected DefaultListModel dtmCliente;
    protected DefaultListModel dtmInstructor;
    protected DefaultListModel dtmActividad;
    protected DefaultListModel dtmInstVerSocios;
    protected DefaultListModel dtmInstVerActividades;

    // Menu
    JMenuItem itemConectar;
    JMenuItem itemSalir;

    /**
     * Constructor de clase vacío que inizializa lo necesario
     */
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(new Dimension(frame.getWidth()+60, frame.getHeight()+30));
        frame.setLocationRelativeTo(null);
        asignarDlm();
        activarMenu();

        for(LugarAct constant : LugarAct.values()) { actComboLugar.addItem(constant.getValor()); }
        actComboLugar.setSelectedIndex(-1);
    }

    /**
     * Asigna los dlm a las Jlist
     */
    private void asignarDlm() {
        dtmCliente = new DefaultListModel<>();
        clienteLista.setModel(dtmCliente);

        dtmInstructor = new DefaultListModel<>();
        instList.setModel(dtmInstructor);

        dtmInstVerSocios = new DefaultListModel<>();
        instListVerSocios.setModel(dtmInstVerSocios);

        dtmInstVerActividades = new DefaultListModel<>();
        instListVerAct.setModel(dtmInstVerActividades);

        dtmActividad = new DefaultListModel<>();
        actList.setModel(dtmActividad);
    }

    /**
     * Activa el menú
     */
    private void activarMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        frame.setJMenuBar(menuBar);
    }
}
