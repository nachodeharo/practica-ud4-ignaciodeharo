package com.ignacio.gui;

import com.ignacio.clases.Actividad;
import com.ignacio.clases.Instructor;
import com.ignacio.clases.Socio;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Modelo que nos permitirá conectar con el servidor de MongoDB y operar con sus colecciones y bbddss
 */
public class Modelo {
    private MongoClient cliente;
    private MongoCollection<Document> socios;
    private MongoCollection<Document> instructores;
    private MongoCollection<Document> actividades;

    /**
     * Conecta con el servidor de mongo  mediante cliente y establece la bbdd y las colecciones
     */
    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "Gym";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "socio";
        socios = db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_EMPLEADOS = "instructor";
        instructores = db.getCollection(COLECCION_EMPLEADOS);
        String COLECCION_DEPARTAMENTOS = "actividad";
        actividades = db.getCollection(COLECCION_DEPARTAMENTOS);
    }

    /**
     * Desconecta el cliente de mongo
     */
    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    /**
     * Permite acceder al cliente de mongo
     * @return
     */
    public MongoClient getCliente() {
        return cliente;
    }


    /**
     * Devuelve todos los socios
     * @return arraylist de socios
     */
    public ArrayList<Socio> getSocio() {
        ArrayList<Socio> lista = new ArrayList<>();

        for (Document document : socios.find()) {
            lista.add(documentToSocio(document));
        }
        return lista;
    }

    /**
     * Devuelve todos los socios de X instructor
     * @return arraylist de socios de X instructor
     */
    public ArrayList<Socio> getSocioInstructor(String comparador) {
        ArrayList<Socio> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("idInstructor", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : socios.find(query)) {
            lista.add(documentToSocio(document));
        }

        return lista;
    }

    /**
     * Devuelve todos las actividaddes
     * @return arraylist de las actividaddes
     */
    public ArrayList<Actividad> getActividad() {
        ArrayList<Actividad> lista = new ArrayList<>();

        for (Document document : actividades.find()) {
            lista.add(documentToActividad(document));
        }
        return lista;
    }

    /**
     * Devuelve todos las actividaddes de X instructor
     * @return arraylist de las actividaddes de X instructor
     */
    public ArrayList<Actividad> getActividadInstructor(String comparador) {
        ArrayList<Actividad> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("idInstructor", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : actividades.find(query)) {
            lista.add(documentToActividad(document));
        }

        return lista;
    }

     /**
     * Devuelve todos lOS instructores
     * @return arraylist de los instructores
     */
    public ArrayList<Instructor> getInstructor() {
        ArrayList<Instructor> lista = new ArrayList<>();

        for (Document document : instructores.find()) {
            lista.add(documentToInstructor(document));
        }
        return lista;
    }

    /**
     * Devuelve todos los socios que contengan X letras del DNI
     * @param comparador el dni a comprarar
     * @return arrayList de socios con x dni
     */
    public ArrayList<Socio> getSocioDni(String comparador) {
        ArrayList<Socio> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("dni", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : socios.find(query)) {
            lista.add(documentToSocio(document));
        }

        return lista;
    }


    /**
     * Guarda el objeto (Inserta) en mongo
     * @param obj
     */
    public void guardarObjeto(Object obj) {
        if (obj instanceof Socio) {
            socios.insertOne(objectToDocument(obj));
        } else if (obj instanceof Instructor) {
            instructores.insertOne(objectToDocument(obj));
        } else if (obj instanceof Actividad) {
            actividades.insertOne(objectToDocument(obj));
        }
    }

    /**
     * Permite la modificación de un objeto de la bbdd (Modifica)
     * @param obj el objeto a modificar
     */
    public void modificarObjeto(Object obj) {
        if (obj instanceof Socio) {
            Socio socio = (Socio) obj;
            socios.replaceOne(new Document("_id", socio.getId()), objectToDocument(socio));
        } else if (obj instanceof Instructor) {
            Instructor instructor = (Instructor) obj;
            instructores.replaceOne(new Document("_id", instructor.getId()), objectToDocument(instructor));
        } else if (obj instanceof Actividad) {
            Actividad actividad = (Actividad) obj;
            actividades.replaceOne(new Document("_id", actividad.getId()), objectToDocument(actividad));
        }
    }

    /**
     * Permite la eliminación de un objeto de la bbdd (Elimina)
     * @param obj el objeto a eliminar
     */
    public void eliminarObjeto(Object obj) {
        if (obj instanceof Socio) {
            Socio socio = (Socio) obj;
            socios.deleteOne(objectToDocument(socio));
        } else if (obj instanceof Instructor) {
            Instructor instructor = (Instructor) obj;
            instructores.deleteOne(objectToDocument(instructor));
        } else if (obj instanceof Actividad) {
            Actividad actividad = (Actividad) obj;
            actividades.deleteOne(objectToDocument(actividad));
        }
    }

    /**
     * Permite pasar los datos de un objeto a un documento para ser utilizado por mongo
     * @param obj el objeto a convertir
     * @return
     */
    private Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Socio) {
            Socio socio = (Socio) obj;

            dc.append("nombre", socio.getNombre());
            dc.append("apellidos", socio.getApellidos());
            dc.append("dni", socio.getDni());
            dc.append("fechaAlta", socio.getFechaAlta().toString());
            dc.append("idInstructor", socio.getIdInstructor());
        } else if (obj instanceof Instructor) {
            Instructor instructor = (Instructor) obj;

            dc.append("nombre", instructor.getNombre());
            dc.append("apellidos", instructor.getApellidos());
            dc.append("codInst", instructor.getCodIns());

        } else if (obj instanceof Actividad) {
            Actividad actividad = (Actividad) obj;

            dc.append("titulo", actividad.getTitulo());
            dc.append("precio", actividad.getPrecio());
            dc.append("lugar", actividad.getLugar());
            dc.append("idInstructor", actividad.getIdInstructor());
        } else {
            return null;
        }
        return dc;
    }

    /**
     * Pasa de documento a la clase Instructor
     * @param doc el documento del que pasamos
     * @return Un Instructor
     */
    private Instructor documentToInstructor(Document doc) {
        Instructor instructor = new Instructor();

        instructor.setId(doc.getObjectId("_id"));
        instructor.setNombre(doc.getString("nombre"));
        instructor.setApellidos(doc.getString("apellidos"));
        instructor.setCodIns(doc.getString("codInst"));

        return instructor;
    }


    /**
     * Pasa de documento a la clase Actividad
     * @param doc el documento del que pasamos
     * @return Una Actividad
     */
    private Actividad documentToActividad(Document doc) {
        Actividad actividad = new Actividad();

        actividad.setId(doc.getObjectId("_id"));
        actividad.setTitulo(doc.getString("titulo"));
        actividad.setPrecio(doc.getDouble("precio"));
        actividad.setLugar(doc.getString("lugar"));
        actividad.setIdInstructor(doc.getString("idInstructor"));
        return actividad;
    }

    /**
     * Pasa de documento a la clase Socio
     * @param doc el documento del que pasamos
     * @return Un Socio
     */
    private Socio documentToSocio(Document doc) {
        Socio socio = new Socio();

        socio.setId(doc.getObjectId("_id"));
        socio.setNombre(doc.getString("nombre"));
        socio.setApellidos(doc.getString("apellidos"));
        socio.setDni(doc.getString("dni"));
        socio.setFechaAlta(LocalDate.parse(doc.getString("fechaAlta")));
        socio.setIdInstructor(doc.getString("idInstructor"));
        return socio;
    }

}
