package com.ignacio.utils;

import javax.swing.*;

/**
 * Clase que contiene métodos que pueden ser necesitados desde cualquier otra clase.
 *
 * @author Ignacio de Haro con base de Elena Jimenez
 */
public class Util {
    /**
     * Muestra el mensaje de error deseado en una ventana emergente.
     *
     * @param mensaje Representa el texto deseado para el mensaje de error.
     */
    public static void mostrarMensajeError(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     *
     * @param mensaje permite devolver un String preguntando mediante una ventana emergente una pregunta por parámetro
     * @return String de la respuests
     */
    public static String preguntarMensaje(String mensaje){

        String question = JOptionPane.showInputDialog(null, mensaje, "Responde", JOptionPane.QUESTION_MESSAGE);
        return question;
    }
}
