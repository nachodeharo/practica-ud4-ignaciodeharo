package com.ignacio.clases;

import org.bson.types.ObjectId;

/**
 * Clase Instructor que permite crear colecciones (Y documentos), con la forma de clase
 */
public class Instructor {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private String codIns;


    /**
     * Constructor de clase con todos los atributos
     * @param id del instructor
     * @param nombre nombre del instructor
     * @param apellidos apellidos del instructor
     * @param codIns codigo del instructor
     */
    public Instructor(ObjectId id, String nombre, String apellidos, String codIns) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.codIns = codIns;
    }

    /**
     * Constructor vacío de clase para instanciar vacío
     */
    public Instructor() {
    }

    /**
     * Método toString sobreescrito que nos permite visualizar datos a nuestra manera
     * @return cadena de texto formateada
     */
    @Override
    public String toString() {
        return nombre + " - " + apellidos + " - " + codIns;
    }

    /**
     * Getter id
     * @return id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Setter id
     * @param id la id del instructor
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Getter Nombre
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter nombre
     * @param nombre del instructor
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Getter de los apellidos
     * @return los apellidos del instructor
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Setter de los apellidos
     * @param apellidos apellidos del instructor
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Getter del codigo de instructor
     * @return del codigo de instructor
     */
    public String getCodIns() {
        return codIns;
    }

    /**
     * Setter del codigo de instructor
     * @param codIns codigo del instructor
     */
    public void setCodIns(String codIns) {
        this.codIns = codIns;
    }
}
