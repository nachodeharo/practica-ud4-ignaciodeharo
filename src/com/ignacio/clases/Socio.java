package com.ignacio.clases;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * Clase Socio que permite crear colecciones (Y documentos), con la forma de clase
 */
public class Socio {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private String dni;
    private LocalDate fechaAlta;
    private String idInstructor;

    /**
     * Constructor completo de Socio, que permite crear un socio completo
     * @param id del socio
     * @param nombre del socio
     * @param apellidos del socio
     * @param dni del socio
     * @param fechaAlta del socio
     * @param idInstructor del socio
     */
    public Socio(ObjectId id, String nombre, String apellidos, String dni, LocalDate fechaAlta, String idInstructor) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.fechaAlta = fechaAlta;
        this.idInstructor = idInstructor;
    }

    /**
     * Constructor vacío para permitir instanciar la clase
     */
    public Socio() {
    }

    /**
     * Método toString sobreescrito que nos permite visualizar datos a nuestra manera
     * @return cadena de texto formateada
     */
    @Override
    public String toString() {
        return nombre + " - " + apellidos + " - " + dni;
    }

    /**
     * Getter id
     * @return id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Setter de la id
     * @param id la id
     */
    public void setId(ObjectId id) {
        this.id = id;
    }


    /**
     * Getter nombre
     * @return nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Setter nombre
     * @param nombre del socio
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    /**
     * Getter Apellidos
     * @return apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Getter dni
     * @return dni
     */

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Getter fecha alta
     * @return fecha alta
     */
    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }


    /**
     * Getter id instructor
     * @return id instructor
     */
    public String getIdInstructor() {
        return idInstructor;
    }

    public void setIdInstructor(String idInstructor) {
        this.idInstructor = idInstructor;
    }
}
