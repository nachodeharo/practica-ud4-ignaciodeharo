package com.ignacio.clases;

import org.bson.types.ObjectId;


/**
 * Clase Actividad que permite la creacion de colecciones con la estructura de Actividad
 */
public class Actividad {
    private ObjectId id;
    private String titulo;
    private double precio;
    private String lugar;
    private String idInstructor;


    /**
     * Constructor de clase Actividad con todos los campos
     * @param id el obj id de una actividad
     * @param titulo de la actividad
     * @param precio de la actividad
     * @param lugar de la actividad (Instalación)
     * @param idInstructor id del instructor encargado
     */
    public Actividad(ObjectId id, String titulo, double precio, String lugar, String idInstructor) {
        this.id = id;
        this.titulo = titulo;
        this.precio = precio;
        this.lugar = lugar;
        this.idInstructor = idInstructor;
    }

    /**
     * Constructor vacío de clase para instanciar sin settear datos
     */
    public Actividad(){

    }

    /**
     * Método toString sobreescrito que permite visualizar los datos
     * @return la cadena de texto de datos
     */
    @Override
    public String toString() {
        return titulo + " - " + precio + " - " + lugar;
    }


    /**
     * Getter de id
     * @return la id
     */
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Getter de titulo
     * @return la titulo
     */
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Getter de precio
     * @return la precio
     */
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Getter de lugar
     * @return la lugar
     */
    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    /**
     * Getter de la id Instructor
     * @return la id Instructor
     */
    public String getIdInstructor() {
        return idInstructor;
    }

    public void setIdInstructor(String idInstructor) {
        this.idInstructor = idInstructor;
    }
}
